package com.anto.kycapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.AllRequestTo;
import com.anto.kycapp.Models.LoginAgentInputTO;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.DeviceUtils;
import com.anto.kycapp.Utils.FontChangeCrawler;
import com.anto.kycapp.Utils.RequestResponseUtils;
import com.google.gson.Gson;



import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LoginActivity extends Activity {

    Button submit_button;
    TextView otp_title;
    EditText et_otp,et_email;
    String email;
    int i =0;
    private static List<String> menulist = new ArrayList<String>();
    public static String authtype ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FontChangeCrawler fontChanger = new FontChangeCrawler(this.getAssets(), this.getResources().getString(R.string.appfont));
        FontChangeCrawler fontChangerBold = new FontChangeCrawler(this.getAssets(), this.getResources().getString(R.string.appfontBold));
        fontChanger.replaceFonts((ViewGroup)findViewById(R.id.main_content));
        fontChangerBold.replaceFontsTextView((TextView)findViewById(R.id.login_txt));

        otp_title = (TextView)findViewById(R.id.otp_title);
        et_otp = (EditText)findViewById(R.id.et_otp);
        et_email = (EditText)findViewById(R.id.et_email);

        submit_button = (Button)findViewById(R.id.submit_button);

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkAvailable(LoginActivity.this)) {
                    if (i == 0) {
                        getOtp();
                    } else {
                        getLogin();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Check your network", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    public void getOtp(){
        Constants.showLoading(this);
        if (validateEmail()) {

            email = et_email.getText().toString();
            //System.out.println("The email is"+email);
            Constants.getEkycServicesAPI(LoginActivity.this,"yes").validateUser(email.trim(), new Callback<AllRequestTo>() {
                @Override
                public void success(AllRequestTo responseTo, Response response) {
                    try {
                        String reqbody = RequestResponseUtils.decrypt(responseTo.getEncryptedpayload(),Constants.getaesprimaryFromShared(LoginActivity.this,responseTo));
                        AgentLoginResponseTO agentLoginResponseTO = (AgentLoginResponseTO) DeviceUtils.getJsonObj(reqbody,AgentLoginResponseTO.class);
                        Constants.dismissloading();
                        if (!Constants.checkIfException(LoginActivity.this,agentLoginResponseTO)) {
                            i = 1;
                            try {
                                authtype = agentLoginResponseTO.getResult().getAuthType();
                                if (agentLoginResponseTO.getResult().getAuthType().equals("STATIC")) {
                                    submit_button.setText(R.string.login);
                                    otp_title.setVisibility(View.VISIBLE);
                                    otp_title.setText(R.string.password_txt);
                                   //et_otp.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                    et_otp.setVisibility(View.VISIBLE);
                                } else {
                                    submit_button.setText(R.string.login);
                                    otp_title.setVisibility(View.VISIBLE);
                                    otp_title.setText(R.string.otp);
                                   // et_otp.setInputType(InputType.TYPE_CLASS_PHONE);
                                    et_otp.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Constants.dismissloading();
                    Constants.ShowErrorHandlerMessage(LoginActivity.this,error);
                }
            });

        } else {
            Constants.dismissloading();
        }
    }

    public void getLogin(){
        Constants.showLoading(this);
        if (validateEmail() && validateOtp()) {
            LoginAgentInputTO loginAgentInputTO = new LoginAgentInputTO();
            loginAgentInputTO.setUsername(et_email.getText().toString().trim());
            loginAgentInputTO.setPassword(et_otp.getText().toString().trim());
            loginAgentInputTO.setAuthtype(authtype);
            String requestcontent = DeviceUtils.getJsonFormat(loginAgentInputTO);

            final AllRequestTo allRequestTo = new AllRequestTo();
            try {
                String primary = RequestResponseUtils.getPrimary();
                String reqcontent = RequestResponseUtils.encrypt(requestcontent,primary);
                allRequestTo.setEncryptedKey(RequestResponseUtils.encryptMessage(primary,this));
                allRequestTo.setEncryptedpayload(reqcontent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Constants.getEkycServicesAPI(LoginActivity.this,"yes").loginKyc(allRequestTo, new Callback<AllRequestTo>() {
                @Override
                public void success(AllRequestTo responseTo, Response response) {
                    try {
                        String reqbody = RequestResponseUtils.decrypt(responseTo.getEncryptedpayload(),Constants.getaesprimaryFromShared(LoginActivity.this,responseTo));
                        AgentLoginResponseTO agentLoginResponseTO = (AgentLoginResponseTO) DeviceUtils.getJsonObj(reqbody,AgentLoginResponseTO.class);

                        Constants.dismissloading();
                        if (!Constants.checkIfException(LoginActivity.this,agentLoginResponseTO)) {
                            try {
                                UserdataInputTo userdataInputTo = new UserdataInputTo();
                                userdataInputTo.setBusiness(agentLoginResponseTO.getResult().getBusinessDetails().get(0).getName());
                                userdataInputTo.setAuthLevel(agentLoginResponseTO.getResult().getBusinessDetails().get(0).getNoOfAuthLevel());
                                userdataInputTo.setBankName(agentLoginResponseTO.getResult().getBusinessDetails().get(0).getBankName());
                                userdataInputTo.setStatus(agentLoginResponseTO.getResult().getApplicationDetails().get(0).getData().getRole().getRoleLevel());
                                userdataInputTo.setUserType(agentLoginResponseTO.getResult().getLoginDetail().getUserType());
                                //System.out.println("BanKname"+agentLoginResponseTO.getResult().getBusinessDetails().get(0).getBankName());
                                for (int i=0; i< agentLoginResponseTO.getResult().getApplicationDetails().get(0).getMenuDatas().size(); i++) {
                                    //System.out.println("Menu Name"+ agentLoginResponseTO.getResult().getApplicationDetails().get(0).getMenuDatas().get(i).getMenuName());
                                  /*  if(i==1)
                                        menulist.add("Vehicle Signup");*/
                                    menulist.add(agentLoginResponseTO.getResult().getApplicationDetails().get(0).getMenuDatas().get(i).getMenuName());
                                }
                                menulist.add("Capture KYC");
                                menulist.add("Logout");
                                //System.out.println("The token"+agentLoginResponseTO.getResult().getToken());
                                //System.out.println("The menu pref size"+Constants.getSharedPreferenceStringList(LoginActivity.this,Constants.MENU_KEY).size());
                                if (Constants.getSharedPreferenceStringList(LoginActivity.this,Constants.MENU_KEY).size() == 0) {
                                    Constants.setSharedPreferenceStringList(LoginActivity.this,Constants.MENU_KEY,menulist);
                                }
                                Constants.setSharedPref(LoginActivity.this, getString(R.string.tokenkey), "logintype", "loggedin");
                                Constants.setSharedPref(LoginActivity.this, getString(R.string.tokenkey), "agentid", et_email.getText().toString().trim());
                                Constants.setSharedPref(LoginActivity.this, getString(R.string.tokenkey), "authlevel", agentLoginResponseTO.getResult().getBusinessDetails().get(0).getNoOfAuthLevel());
                                Constants.setSharedPref(LoginActivity.this,getString(R.string.tokenkey),"securitytoken",agentLoginResponseTO.getResult().getToken());

                                Gson gson = new Gson();
                                String json = gson.toJson(userdataInputTo);
                                Constants.setSharedPref(LoginActivity.this,getString(R.string.tokenkey),"usermodelclass",json);

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Constants.toastMessage(LoginActivity.this,"Exception Occurred");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Constants.dismissloading();
                    Constants.ShowErrorHandlerMessage(LoginActivity.this,error);
                }
            });

        }
        else{
            Constants.dismissloading();
        }
    }


    public boolean validateEmail(){
        if(et_email.getText().toString().length()>0){
            return true;
        }
        else
        {
            Toast.makeText(this, "Enter valid email", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public boolean validateOtp(){
        if(et_otp.getText().toString().length()>0)
        {
            return true;
        }
        else
        {
            Toast.makeText(this, "Enter valid otp", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
