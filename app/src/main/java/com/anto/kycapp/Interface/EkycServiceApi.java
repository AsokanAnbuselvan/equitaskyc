package com.anto.kycapp.Interface;



import com.anto.kycapp.Models.AadharValidationResponseTo;
import com.anto.kycapp.Models.AddkycAgentInputTO;
import com.anto.kycapp.Models.AddkycAgentResponseTO;
import com.anto.kycapp.Models.AdduserrequestTO;
import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.AllRequestTo;
import com.anto.kycapp.Models.EditKycResponseTO;
import com.anto.kycapp.Models.FetchByKitInputTO;
import com.anto.kycapp.Models.GenerateOtpResponseTo;
import com.anto.kycapp.Models.GetKycDetailResponseTO;
import com.anto.kycapp.Models.GetKycRequestTo;
import com.anto.kycapp.Models.KycdataInputTo;
import com.anto.kycapp.Models.LoginAgentInputTO;
import com.anto.kycapp.Models.OtpResponseTo;
import com.anto.kycapp.Models.OtpkycValidationResponseTo;
import com.anto.kycapp.Models.PanValidationResponseTo;
import com.anto.kycapp.Models.PendingSignupResponseTo;
import com.anto.kycapp.Models.PendingkycResponseTo;
import com.anto.kycapp.Models.PidBlockModels.PidblockModels;
import com.anto.kycapp.Models.RegisterCustomerResponseTO;
import com.anto.kycapp.Models.RegistrationRequestTo;
import com.anto.kycapp.Models.UpdateEntityRequestTo;
import com.anto.kycapp.Models.UserDetailsResponseTo;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.Models.ValidateBioResponseTo;
import com.anto.kycapp.Models.ValidateOtpRequestTo;
import com.anto.kycapp.Models.ValidateOtpResponseTo;
import com.anto.kycapp.Models.ValidatecardandkitRequestTo;
import com.anto.kycapp.Models.ValidatecardandkitResponseTo;
import com.anto.kycapp.Models.ValidationRequestTo;
import com.anto.kycapp.Utils.Constants;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;


/**
 * Created by anto on 10/5/2018.
 */

public interface EkycServiceApi {

    //login
    @POST(Constants.serviceUrls.LOGIN_KYC)
    public void loginKyc(@Body AllRequestTo loginAgentInputTO, Callback<AllRequestTo> agentLoginResponseTOCallback);

    //login
    @GET(Constants.serviceUrls.VALIDATE_USER)
    public void validateUser(@Query("username") String username,Callback<AllRequestTo> agentLoginResponseTOCallback);

    //Fetch OTP
    @GET(Constants.serviceUrls.OTP_AGENT)
    public void fetchOTP(@Query("userName") String userName, @Query("generateOtp") boolean generateOtp, Callback<OtpResponseTo> otpResponseToCallback);


    //Resend OTP
    @GET(Constants.serviceUrls.RESENDOTP_AGENT)
    public void resendOTP(@Query("userName") String userName, Callback<OtpResponseTo> otpResponseToCallback);


    //Add kyc user
    @POST(Constants.serviceUrls.ADDUSER)
    public void adduser(@Body AdduserrequestTO adduserrequestTO, Callback<AgentLoginResponseTO> agentLoginResponseTOCallback);

    //Forex Register
    @POST(Constants.serviceUrls.FOREX_REGISTER)
    void registerCustomer(@Body RegistrationRequestTo registrationRequestTo, Callback<RegisterCustomerResponseTO> responseTOCallback);

    //Fetch Pending Signup
    @GET(Constants.serviceUrls.FETCH_PENDINGSIGNUP)
    void fetchPendingSignup(@Query("business") String business,@Query("authLevel") String authlevel,@Query("status") String status,@Query("userType") String userType, Callback<PendingSignupResponseTo> pendingSignupResponseToCallback);

    //Check customer signup
    @GET(Constants.serviceUrls.CHECK_USER)
    void checkuserExists(@Path("customerid") String customerid, Callback<AgentLoginResponseTO> agentLoginResponseTOCallback);


    @POST(Constants.serviceUrls.VALIDATECARDANDKIT)
    void validatecardandkit(@Body ValidatecardandkitRequestTo validatecardandkitRequestTo, Callback<ValidatecardandkitResponseTo> validatecardandkitResponseToCallback);

    @POST(Constants.serviceUrls.FETCH_PENDINGKYC)
    void fetchPendingKyc(@Body AllRequestTo kycdataInputTo, Callback<AllRequestTo> pendingkycResponseToCallback);

    //getuser
    @GET(Constants.serviceUrls.GETUSERDETAILS)
    void getUserDetails(@Path("id") String id, Callback<UserDetailsResponseTo> userDetailsResponseToCallback);

    //Add ekyc agent
    @POST(Constants.serviceUrls.ADDKYC_AGENT)
    void addekyc(@Body AddkycAgentInputTO addkycAgentInputTO , Callback<AddkycAgentResponseTO> addkycAgentResponseTOCallback);

    //get kyc details
    @POST(Constants.serviceUrls.GETKYCDETAILS)
    void getKycDetails(@Body AllRequestTo getKycRequestTo , Callback<AllRequestTo> getKycDetailResponseTOCallback);

    //validatePan
    @POST(Constants.serviceUrls.PANVALIDATION)
    void validatePan(@Body ValidationRequestTo validationRequestTo,Callback<PanValidationResponseTo> callback);

    //generateAadhaarOtp
    @POST(Constants.serviceUrls.GENERATEOTP)
    void generateAadhaarOtp(@Body ValidationRequestTo validationRequestTo,Callback<AadharValidationResponseTo> callback);

    //generateAadhaarOtp
    @POST(Constants.serviceUrls.GENERATEOTP)
    void generateAadhaarOtpkyc(@Body ValidationRequestTo validationRequestTo,Callback<OtpkycValidationResponseTo> callback);

    //validateAadhaarOtp
    @POST(Constants.serviceUrls.VALIDATEOTP)
    void validateAadhaarOtp(@Body ValidationRequestTo validationRequestTo,Callback<OtpResponseTo> callback);

    //validateAadhaarOtp
    @POST(Constants.serviceUrls.VALIDATEOTP)
    void validateAadhaarOtpkyc(@Body ValidationRequestTo validationRequestTo,Callback<OtpkycValidationResponseTo> callback);

    //fetchbykit
    @POST(Constants.serviceUrls.FETCHBYKIT)
    void fetchByKit(@Body AllRequestTo fetchByKitInputTO, Callback<AllRequestTo> callback);

    //validateBio
    @POST(Constants.serviceUrls.VALIDATEBIO)
    void validateBio(@Body AllRequestTo pidblockModels,Callback<AllRequestTo> callback);

    //Generateotp
    @GET(Constants.serviceUrls.GENERATE_OTP)
    void generateOtp(@Path("customerid") String customerid,Callback<AllRequestTo> callback);

    //validateOtp
    @POST(Constants.serviceUrls.VALIDATE_OTP)
    void validateOtp(@Body AllRequestTo validateOtpRequestTo,Callback<AllRequestTo> callback);

    //updateentity
    @POST(Constants.serviceUrls.UPDATE_ENTITY)
    void updateUser(@Body AllRequestTo updateEntityRequestTo,Callback<AllRequestTo> callback);

    //ValidateBusiness
    @POST(Constants.serviceUrls.VALIDATE_BUSINESS)
    void validateBusiness(Callback<AllRequestTo> allRequestToCallback);
}
