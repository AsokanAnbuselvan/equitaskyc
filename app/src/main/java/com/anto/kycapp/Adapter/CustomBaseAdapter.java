package com.anto.kycapp.Adapter;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.anto.kycapp.Models.PendingsignupResponse;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.FontChangeCrawler;


import java.util.ArrayList;

/**
 * Created by anto on 2/14/2018.
 */

public class CustomBaseAdapter extends BaseAdapter {
    ArrayList<PendingsignupResponse> myList = new ArrayList();
    LayoutInflater inflater;
    FragmentActivity context;

    public CustomBaseAdapter(FragmentActivity context, ArrayList<PendingsignupResponse> myList) {
        this.myList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }


    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public PendingsignupResponse getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_list_item, parent, false);

            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), context.getResources().getString(R.string.appfontBold));
        fontChanger.replaceFonts((ViewGroup)convertView.findViewById(R.id.listitemlay));


        mViewHolder.corporate_txt.setText(myList.get(position).getBusiness());
        mViewHolder.customerid_txt.setText(myList.get(position).getCustomerId());
        mViewHolder.dateandtime_txt.setText(myList.get(position).getCreated());
        mViewHolder.status_txt.setText(myList.get(position).getStatus());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myList.get(position).getStatus().equals("DECLINED")) {
                   /* context.getSupportFragmentManager().beginTransaction()
                            .addToBackStack("mainactivity")
                            .add(R.id.pendingsignuplay, new UserRegisterDetailFragment(myList.get(position).getId(),myList.get(position).getBusiness())).commit();*/
                }
            }
        });

        //Toast.makeText(context, myList.get(position).getStatus(), Toast.LENGTH_SHORT).show();
        return convertView;
    }

    private class MyViewHolder {
        TextView corporate_txt, customerid_txt,dateandtime_txt,status_txt;
        RelativeLayout content_baselay;
        ImageView img_trans;

        public MyViewHolder(View item) {
            corporate_txt = item.findViewById(R.id.corporate_txt);
            customerid_txt = item.findViewById(R.id.customerid_txt);
            dateandtime_txt = item.findViewById(R.id.dateandtime_txt);
            status_txt = item.findViewById(R.id.status_txt);
        }
    }


}
