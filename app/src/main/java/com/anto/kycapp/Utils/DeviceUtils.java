package com.anto.kycapp.Utils;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.gson.Gson;

import java.io.File;

/**
 * Created by anto on 1/19/2019.
 */

public class DeviceUtils {

    public Boolean isDeviceRooted(Context context){
       // boolean isRooted = isrooted1() || isrooted2();
        return isrooted1();
    }

    private boolean isrooted1() {

        String[] paths = { "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    public static String getJsonFormat(Object object) {
        Gson gson = new Gson();
        String jsonFormat = gson.toJson(object);
        return jsonFormat;
    }

    public static Object getJsonObj(String jsonstr, Class modelclass) {
        Gson gson = new Gson();
        return gson.fromJson(jsonstr, modelclass);
    }

    public static void setCopyPasteEnableFalse(EditText editText){
        editText.setLongClickable(false);
        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
            public void onDestroyActionMode(ActionMode mode) {
            }
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
    }
}
