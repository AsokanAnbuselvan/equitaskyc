package com.anto.kycapp.Utils;

import android.app.Activity;
import android.util.Base64;

import com.anto.kycapp.R;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class RequestResponseUtils {

    // Get RSA keys. Uses key size of 2048.
    public static HashMap<String,Object> getRSAKeys() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4);
        SecureRandom secureRandom = new SecureRandom();
        byte[] byteArr = new byte[20];
        secureRandom.nextBytes(byteArr);
        keyPairGenerator.initialize(spec,secureRandom);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        HashMap<String, Object> keys = new HashMap<String,Object>();
        keys.put("private", privateKey);
        keys.put("public", publicKey);


        return keys;
    }


    // Decrypt using RSA privateKey
    public static String decryptPrimary(String encryptedText,String resprimary, Activity activity) throws Exception {
        PrivateKey privateKey = convertBytesToPrivateKey(Base64.decode(resprimary.getBytes("UTF-8"),Base64.DEFAULT));
        Cipher cipher = Cipher.getInstance(activity.getString(R.string.respadding));
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(Base64.decode(encryptedText, Base64.DEFAULT)));
    }

    // Decrypt using RSA privateKey
    public static String decryptMessage(String encryptedText, Activity activity) throws Exception {
        String resprimary = Constants.getresprimaryFromShared(activity);
        PrivateKey privateKey = convertBytesToPrivateKey(Base64.decode(resprimary.getBytes("UTF-8"),Base64.DEFAULT));
        Cipher cipher = Cipher.getInstance(activity.getString(R.string.respadding));
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(Base64.decode(encryptedText, Base64.DEFAULT)));
    }


    // Encrypt using RSA private key
    public static String encryptMessage(String plainText, Activity activity) throws Exception {
        String reqprimary = Constants.getreqprimaryFromShared(activity);
        PublicKey publicKey = convertBytesToPublicKey(Base64.decode(reqprimary.getBytes("UTF-8"),Base64.DEFAULT));
        Cipher cipher = Cipher.getInstance(activity.getString(R.string.respadding));
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return Base64.encodeToString(cipher.doFinal(plainText.getBytes("UTF-8")), Base64.DEFAULT);
    }

    public static PrivateKey convertBytesToPrivateKey(byte[] privateKeyBytes) throws NoSuchAlgorithmException, InvalidKeySpecException {

        KeyFactory factory = KeyFactory.getInstance("RSA");
        RSAPrivateKey privateKey = (RSAPrivateKey) factory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        return privateKey;
    }

    public static PublicKey convertBytesToPublicKey(byte[] publicKeyBytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        KeyFactory factory = KeyFactory.getInstance("RSA");
        RSAPublicKey publicKey = (RSAPublicKey) factory.generatePublic(new X509EncodedKeySpec(publicKeyBytes));
        return publicKey;
    }



    //Encrypt AES
    public static  String encrypt(String plainText, String key) throws Exception
    {
        Cipher cipher = getCipher(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());

        return Base64.encodeToString(encryptedBytes,Base64.DEFAULT);
    }

    //Decrypt AES
    public static String decrypt(String encrypted, String key) throws Exception
    {
        Cipher cipher = getCipher(Cipher.DECRYPT_MODE, key);
        byte[] plainBytes = cipher.doFinal(Base64.decode(encrypted,Base64.DEFAULT));

        return new String(plainBytes);
    }

    private static  Cipher getCipher(int cipherMode, String key)
            throws Exception
    {
        String encryptionAlgorithm = "AES";
        SecretKeySpec keySpecification = new SecretKeySpec(
                key.getBytes("UTF-8"), encryptionAlgorithm);
        Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
        cipher.init(cipherMode, keySpecification);

        return cipher;
    }

    public static String getPrimary(){
        String primary  = UUID.randomUUID().toString();
        primary = primary.replaceAll("-","");
        return primary;
    }

}
