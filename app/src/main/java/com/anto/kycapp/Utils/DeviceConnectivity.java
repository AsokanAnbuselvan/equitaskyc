package com.anto.kycapp.Utils;
import org.apache.commons.codec.binary.Base64;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.widget.Toast;

import com.anto.kycapp.Models.Opts;
import com.anto.kycapp.Models.Pi;

import com.anto.kycapp.Models.PidOptions;
import com.google.gson.Gson;

import org.json.JSONException;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class DeviceConnectivity {
    public static int mantraRequestCode = 5;
    public static int morphoRequestCode = 6;

    public static boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void morphodevice(Activity activity){

    }



    public static String getPIDOptions() {
        String xml = "<PidData> <Data type=\"X\">MjAyMC0wNi0xNVQxOTozMTo1MyjQFqf74XPDddBsIH6qJDuHrqCsuSg7KF/CvGJFy/ice7mRwYB7 XsdI/wTSaCCXl7appE0Q3aOsrh9FZq2UcOdAdGQff7i8zLOBHXuxN5M2v44S0zBA0GI1uEtiEgSZ wKujAeZwJob1eXiU/ql+ZiTlU6wn0wzqeGfxOJ19lBQOI2m/N+jRKyZIj8/S6yXHtuEZRfZ8hX8H y2Hb4Hx4lpPmVb/qjulGbsxJKHoT0PtKxD46lFD/8u0yPe47FPbNk872L7yF2eDLBBgdAMTYHVZd NNzCtg/+Zvbgc/NnGDphcas8ZfCZ20BUMRED2eLGjpJvEUVbI5X3ETukrkF26BMg37Sujy5VUuyv TF+Cu9BQfIZY2Gs6p5UfpyQR6iuAhT8Kqs2li00pJNRJTlbUt5Xm2vdF9cwCeLcp2n6kyhS+bX4Y q6PINCFxd5gOjJhg0NRpWTl/ErajMxoCQZOvulsvliQBA8E6HFa4/4kIBsp7nPZNydbqA0xWr1sD JvJ/dw674P9PS1UKOHouSBQbwc6KWhW3xVgDBcX5nvxJ4sfWQOQzOCSh4Cy19hwcsI3aWVDogWCE QYFru7JQY8VgBtWBHBcFStpvm3Itnd1mkOnw7tp5IfpijcScNULizC7y0b7FzNVKYYpqG5npIjip JBStfJ8BtBEDSu7sBKIDG7nhR+LDkMgsPIyymcrpRwsHlkCYhAojYB/LExPjpQRh3o5SoqlJcM4W vuXkD0jJN+atwg3VfQONS7W8Kso/PPr6woOwvyIEqgDqshelMBaPLTXxzl9Q71O221cF7McYuFjJ XkTezCdz/s9c2b+mJJyMcMzvwiuM2Wvt1gVKLVRDyZxCQE+OUmQtTaZC9p1sbkLyZ+EpbdAzowcB 5sXSQ17ZVCLKUcBEcd6rgI4pNdXIHJQyIKSE33mwUaau4PssxXZrWCrjPGc2lX4OwB/gnrORBwcn tUOUXdCWIrlexaNAExNQKgz/75zBMLp3GsuJXenN/oAmFz8= </Data> <DeviceInfo dc=\"1d098b15-8b34-4564-a3fb-6b461c7013bc\" dpId=\"MANTRA.MSIPL\" mc=\"MIIEGTCCAwGgAwIBAgIGAXK4SRsDMA0GCSqGSIb3DQEBCwUAMIHpMSowKAYDVQQDEyFEUyBNYW50cmEgU29mdGVjaCBJbmRpYSBQdnQgTHRkIDUxTTBLBgNVBDMTREIgMjAzIFNoYXBhdGggSGV4YSBvcHBvc2l0ZSBHdWphcmF0IEhpZ2ggQ291cnQgUyBHIEhpZ2h3YXkgQWhtZWRhYmFkMRIwEAYDVQQJEwlBaG1lZGFiYWQxEDAOBgNVBAgTB0d1amFyYXQxEjAQBgNVBAsTCVRlY2huaWNhbDElMCMGA1UEChMcTWFudHJhIFNvZnRlY2ggSW5kaWEgUHZ0IEx0ZDELMAkGA1UEBhMCSU4wHhcNMjAwNjE1MTM0NjQ5WhcNMjAwNzE1MTQwMTM4WjCBsDEkMCIGCSqGSIb3DQEJARYVc3VwcG9ydEBtYW50cmF0ZWMuY29tMQswCQYDVQQGEwJJTjEQMA4GA1UECBMHR1VKQVJBVDESMBAGA1UEBxMJQUhNRURBQkFEMQ4wDAYDVQQKEwVNU0lQTDEeMBwGA1UECxMVQmlvbWV0cmljIE1hbnVmYWN0dXJlMSUwIwYDVQQDExxNYW50cmEgU29mdGVjaCBJbmRpYSBQdnQgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvoGdgsUFhE/ghqrAhLDooUez05e0Ycm8qZJId9ccEE/B/Iq4eACdb9ljsQ2TY72RB13iRLTt9gbS0Ugi99eltT10SIKa4my8gzSmSkMz8+hg8vm0US7gRFZkJAUcnTZAazkTa/zzed4mGp975tj4PP94KAvsdK1EQlVkNN1G1N1o+qzhXgdBSedNzRVwxp4NiPM09UajkjgZeOJTPl2r/RZyPnd6JvfNBS66/gp4gNHPEK/+5Sc/7OQ9dIPGpIT4SwIEa0CRplLYiRwRwqKkI+9A0Q1TZWYkvTEUNrFOio2N4MBesy+rkyFaVWm7wyOVdFcP9EaGDtFr1Xvzv+tN9QIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQA/19LhTGD5zkwrxZKvzxGsx+0iWLOMeJSaXnjeZQnGWDXTNM819zC++ddGGHoMm4N7PFo84eccnfN5AAczOY0VQKhkyWdKq8F+i56HMpH0yr9J+ww19v1YL5rJGWlkr6Nkkus0i2oE6M5eLccKHV2jTYsIjen1SsmPC1etJGUoAUZuT59fwblCQcnAWlj72qNZmR+ObqB4cfOkAqD8NtvxYzMaJ3adKBtSiiR+I6T/v/An+J+DVAaLvHtbFHysrMjsmUCPbpt0RdL2JRiqT8TzeEDFlROx/WssZnXVs7TgCX8Re/XnOQ3Dg5w/jlaOjNf1EyhmCTQsnsDxpr9EJKA2\" mi=\"MFS100\" rdsId=\"MANTRA.AND.001\" rdsVer=\"1.0.3\"> <additional_info> <Param name=\"srno\" value=\"0589745\"/> <Param name=\"sysid\" value=\"867062036262212\"/> <Param name=\"ts\" value=\"2020-06-15T19:31:58+05:30\"/> </additional_info> </DeviceInfo> <Hmac>Bw8AespCn8gPUSZbOyFY8DrKktCj2ezxiMKvbhXdLMRE/DHBdj+ueCmzCvUY79EZ </Hmac> <Resp errCode=\"0\" errInfo=\"Capture Success\" fCount=\"1\" fType=\"0\" iCount=\"0\" iType=\"0\" nmPoints=\"24\" pCount=\"0\" pType=\"0\" qScore=\"78\"/> <Skey ci=\"20200916\">NBdthFwdjcScKYcQxyj0UDeCFVgKnaoALqP4eNJQw7VlaXs1D2A2TiLsf+tpQ3ibI7P0DGDUUewy GRqjrbBEm9Bk3dkXWO2KfUf5rZ2jN/HfJZxDbfngJ303TjmCBhBaEm4XqWojQO3mowMD8yKAi8Iq 8lsni6Ev5rdFbBBwBgCJAnqckKQQ6xQocuxydgdoaczKGfTgkwF582a/Li5JjCYik3rMn9H+Hzc9 hV6SrDwDk7AF7kMMMVI1EK6/GOLBhXk5EpEGDv7ArotYxYq/Q+Lrsx2YXehLzQtPpPomU075QMgB fc0XnXYNTrBWZZiNHyWZeDGXjdOk4eyiqYbOXg== </Skey> </PidData>";
        PidDataConfig.pidBlock = xml;
        PidDataConfig pidDataConfig = new PidDataConfig();
       // System.out.println("devconPidBlock++"+new Gson().toJson(pidDataConfig.getPidBlockModels("TST0888512  ")));

        try {
            int fingerCount = 1;
            int fingerType = 0;
            int fingerFormat = 0;
            String pidVer = "2.0";
            String timeOut = "10000";
            String posh = "UNKNOWN";
            /*if (positions.size() > 0) {
                posh = positions.toString().replace("[", "").replace("]", "").replaceAll("[\\s+]", "");
            }*/
            Opts opts = new Opts();
            opts.fCount = String.valueOf(fingerCount);
            opts.fType = String.valueOf(fingerType);
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = String.valueOf(fingerFormat);
            opts.pidVer = pidVer;
            opts.timeout = timeOut;
            //opts.otp = "123456";
            opts.wadh = getWadh();

            opts.posh = posh;
            opts.env = "PP";

            Pi pi = new Pi();
            pi.ms = "P";
            pi.mv = "Name";
            pi.lname = "lname";
            pi.lmv = "";
            pi.gender = "M";
            pi.dob = "";
            pi.dobt = "V";
            pi.age = "";
            pi.phone = "";
            pi.email = "";


            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = "1.0";
            pidOptions.Opts = opts;
            pidOptions.Pi = pi;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);

            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getWadh(){
       // String wadhstr = Constants.getTimestamp()+"2.5"+Constants.getTimestamp()+"F"+"Y"+"N"+"N"+"Y";
        String wadhstr = "2.5FYNNY";
        return getSHA256Hash(wadhstr);
    }


    private static String getSHA256Hash(String data) {
        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
            return bytesToString(hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String bytesToString(byte[] hash) {
        return new String(Base64.encodeBase64(hash));
    }




  /* public static String getSha256Hash(String wadhstr) {
        try {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e1) {
                e1.printStackTrace();
            }
            digest.reset();
            return bin2hex(digest.digest(wadhstr.getBytes()));
        } catch (Exception ignored) {
            return null;
        }
    }

    private static String bin2hex(byte[] data) {
        StringBuilder hex = new StringBuilder(data.length * 2);
        for (byte b : data)
            hex.append(String.format("%02x", b & 0xFF));
        return hex.toString();
    } */

}
