package com.anto.kycapp.Utils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.util.Log;

import com.anto.kycapp.Models.PidBlockModels.Data;
import com.anto.kycapp.Models.PidBlockModels.Hmac;
import com.anto.kycapp.Models.PidBlockModels.Meta;
import com.anto.kycapp.Models.PidBlockModels.PidblockModels;
import com.anto.kycapp.Models.PidBlockModels.Skey;
import com.anto.kycapp.Models.PidBlockModels.Uses;
import com.anto.kycapp.Models.ValidationRequestTo;


import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.PublicKey;
import java.util.Date;

import javax.security.cert.X509Certificate;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class PidDataConfig {
    // private static PublicKey publicKey;

    // private Date certExpiryDate;
    public static String pidBlock = "";


    public  String convertToPidBlock(){
     return getUsesTag()+getMetaTag()+getSkeyTag()+getDataTag()+getHmacTag();
    }



    public static Element getElement(String pidBlock){
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factory.setFeature("http://xml.org/sax/features/external-parameter-entities",
                    false);
            factory.setValidating(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(pidBlock)));
            Element rootElement = document.getDocumentElement();
            return rootElement;
        } catch (ParserConfigurationException e) {
            Log.e("TAG", Log.getStackTraceString(e));
        } catch (IOException e) {
            Log.e("TAG", Log.getStackTraceString(e));
        } catch (SAXException e) {
            Log.e("TAG", Log.getStackTraceString(e));
        }
        return null;
    }

    protected static String getNodeValue(String tagName, Element element) {
        NodeList list = element.getElementsByTagName(tagName);

        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();

            if (subList != null && subList.getLength() > 0) {
                return subList.item(0).getNodeValue();
            }
        }

        return null;
    }

    protected static String getString(String tagName,String tagChild, Element element) {
        try {
            NodeList list = element.getElementsByTagName(tagName);
            return list.item(0).getAttributes().getNamedItem(tagChild).getNodeValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


        public  String getUsesTag(){
        Uses uses = new Uses();
        uses.pi = "n";
        uses.pa = "n";
        uses.pfa = "n";
        uses.pin = "n";
        uses.bio = "y";
        uses.bt = "FMR";
        uses.otp = "n";

        try {
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(uses, writer);

            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public PidblockModels getPidBlockModels(String entityid,String idnumber,String signature){
        PidblockModels pidblockModels = new PidblockModels();
        Element element = getElement(pidBlock);
        if (element != null) {
            pidblockModels.setEntityId(entityid);
            pidblockModels.setConsent("Yes");
            pidblockModels.setIdTypeValidation("U");
            pidblockModels.setIdNumber(idnumber);
            pidblockModels.setSignature(signature);
            pidblockModels.setSkey(getNodeValue("Skey",element));
            pidblockModels.setCi(getString("Skey","ci",element));
            pidblockModels.setMc(getString("DeviceInfo","mc",element));
            pidblockModels.setDataType("X");
            pidblockModels.setDataValue(getNodeValue("Data",element));
            pidblockModels.setHmac(getNodeValue("Hmac",element));
            pidblockModels.setMi(getString("DeviceInfo","mi",element));
            pidblockModels.setRdsId(getString("DeviceInfo","rdsId",element));
            pidblockModels.setRdsVer(getString("DeviceInfo","rdsVer",element));
            pidblockModels.setDpId(getString("DeviceInfo","dpId",element));
            pidblockModels.setDc(getString("DeviceInfo","dc",element));
        }

        pidblockModels.getWadh().setVer("2.5");
        pidblockModels.getWadh().setRa("F");
        pidblockModels.getWadh().setRc("Y");
        pidblockModels.getWadh().setLr("N");
        pidblockModels.getWadh().setDe("N");
        pidblockModels.getWadh().setPfr("Y");
        return pidblockModels;
    }

    public String getMetaTag(){

        Element element = getElement(pidBlock);

        Meta meta = new Meta();
        meta.udc = "EQT000000000001";
        if (element != null) {
            meta.rdsId = getString("DeviceInfo","rdsId",element);
            meta.rdsVer = getString("DeviceInfo","rdsVer",element);
            meta.dpId = getString("DeviceInfo","dpId",element);
            meta.dc = getString("DeviceInfo","dc",element);
            meta.mi = getString("DeviceInfo","mi",element);
            meta.mc = getString("DeviceInfo","mc",element);
        }


        try {
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(meta, writer);

            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getSkeyTag(){

        Element element = getElement(pidBlock);

        Skey skey = new Skey();

        if (element != null) {
            skey.ci = getString("Skey","ci",element);
            skey.value = getNodeValue("Skey",element);
           }

        try {
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(skey, writer);

            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getDataTag(){

        Element element = getElement(pidBlock);

        Data data = new Data();

        if (element != null) {
            data.type = "X";
            data.value = getNodeValue("Data",element);
        }

        try {
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(data, writer);

            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getHmacTag(){

        Element element = getElement(pidBlock);

        Hmac hmac = new Hmac();

        if (element != null) {
            hmac.value = getNodeValue("Hmac",element);
        }

        try {
            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(hmac, writer);

            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }





}
