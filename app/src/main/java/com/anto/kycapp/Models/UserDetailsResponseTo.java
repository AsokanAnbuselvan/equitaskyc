package com.anto.kycapp.Models;

/**
 * Created by anto on 3/18/2019.
 */

public class UserDetailsResponseTo extends ResponseTO{

    Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        private String reason;
        private RegistrationData registrationData;
        private KycData kycData;

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public RegistrationData getRegistrationData() {
            return registrationData;
        }

        public void setRegistrationData(RegistrationData registrationData) {
            this.registrationData = registrationData;
        }

        public KycData getKycData() {
            return kycData;
        }

        public void setKycData(KycData kycData) {
            this.kycData = kycData;
        }
    }
}
