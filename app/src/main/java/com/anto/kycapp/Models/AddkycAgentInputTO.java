package com.anto.kycapp.Models;

public class AddkycAgentInputTO {
    String proxyNumber, ekycRefNo;

    public String getProxyNumber() {
        return proxyNumber;
    }

    public void setProxyNumber(String proxyNumber) {
        this.proxyNumber = proxyNumber;
    }

    public String getEkycRefNo() {
        return ekycRefNo;
    }

    public void setEkycRefNo(String ekycRefNo) {
        this.ekycRefNo = ekycRefNo;
    }
}
