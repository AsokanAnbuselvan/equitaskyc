package com.anto.kycapp.Models.PidBlockModels;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "Hmac")
public class Hmac {

    public Hmac() {
    }


    @Text
    public String value;
}
