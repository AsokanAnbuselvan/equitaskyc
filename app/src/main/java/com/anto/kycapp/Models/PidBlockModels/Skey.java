package com.anto.kycapp.Models.PidBlockModels;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "Skey")
public class Skey {

    public Skey() {
    }

    @Attribute(name = "ci", required = false)
    public String ci;

    @Text
    public String value;
}
