package com.anto.kycapp.Models;
public class UpdateEntityRequestTo {

    private String businessId;
    private String businessType;
    private String entityId;
    private String entityType;
    private String fatherName;
    private String gender;
    private String grossAnnualIncome;
    private Boolean isAgriculturalIncome;
    private String maritalStatus;
    private String motherMaidenName;
    private String partnerId;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGrossAnnualIncome() {
        return grossAnnualIncome;
    }

    public void setGrossAnnualIncome(String grossAnnualIncome) {
        this.grossAnnualIncome = grossAnnualIncome;
    }

    public Boolean getIsAgriculturalIncome() {
        return isAgriculturalIncome;
    }

    public void setIsAgriculturalIncome(Boolean isAgriculturalIncome) {
        this.isAgriculturalIncome = isAgriculturalIncome;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

}