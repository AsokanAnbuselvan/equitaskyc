package com.anto.kycapp.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anto on 2/18/2019.
 */

public class PendingSignupResponseTo {


    List<Result> result = new ArrayList<>();

    public List<Result> getResults() {
        return result;
    }

    public void setResults(List<Result> results) {
        this.result = results;
    }

    public static class Result{

        String id,business,created,customerId,maker,status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBusiness() {
            return business;
        }

        public void setBusiness(String business) {
            this.business = business;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMaker() {
            return maker;
        }

        public void setMaker(String maker) {
            this.maker = maker;
        }
    }

}
