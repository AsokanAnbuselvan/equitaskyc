package com.anto.kycapp.Models;

import java.util.List;

/**
 * Created by anto on 2/5/2019.
 */

public class ApplicationDetail {
    Data data;
    private List<MenuData> menuDatas = null;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<MenuData> getMenuDatas() {
        return menuDatas;
    }

    public void setMenuDatas(List<MenuData> menuDatas) {
        this.menuDatas = menuDatas;
    }
}
