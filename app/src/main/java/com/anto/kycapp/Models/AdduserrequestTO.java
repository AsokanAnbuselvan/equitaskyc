package com.anto.kycapp.Models;

/**
 * Created by anto on 2/20/2019.
 */

public class AdduserrequestTO {

    private int status;

    private int authLevel;

    private String userLevel;

    private int roleLevel;

    private String business;

    private String Corporate_Id;

    private String Title;

    private String First_Name;

    private String Last_Name;

    private String Date_Of_Birth;

    private String Gender;

    private String Contact_No;

    private String customerId;

    private String Kit_No;

    private String Card_Type;

    private String Email_Address;

    private String Address_Line1;

    private String Address_Line2;

    private String City;

    private String State;

    private String Country;

    private String Pincode;

    private String Id_Type;

    private String Id_Number;

    private String Expiry_Date;

    private String KYC_Status;

    private String userId;

    private String id;

    private String bank;

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBank() {
        return bank;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setAuthLevel(int authLevel){
        this.authLevel = authLevel;
    }
    public int getAuthLevel(){
        return this.authLevel;
    }
    public void setUserLevel(String userLevel){
        this.userLevel = userLevel;
    }
    public String getUserLevel(){
        return this.userLevel;
    }
    public void setRoleLevel(int roleLevel){
        this.roleLevel = roleLevel;
    }
    public int getRoleLevel(){
        return this.roleLevel;
    }
    public void setBusiness(String business){
        this.business = business;
    }
    public String getBusiness(){
        return this.business;
    }
    public void setCorporate_Id(String Corporate_Id){
        this.Corporate_Id = Corporate_Id;
    }
    public String getCorporate_Id(){
        return this.Corporate_Id;
    }
    public void setTitle(String Title){
        this.Title = Title;
    }
    public String getTitle(){
        return this.Title;
    }
    public void setFirst_Name(String First_Name){
        this.First_Name = First_Name;
    }
    public String getFirst_Name(){
        return this.First_Name;
    }
    public void setLast_Name(String Last_Name){
        this.Last_Name = Last_Name;
    }
    public String getLast_Name(){
        return this.Last_Name;
    }
    public void setDate_Of_Birth(String Date_Of_Birth){
        this.Date_Of_Birth = Date_Of_Birth;
    }
    public String getDate_Of_Birth(){
        return this.Date_Of_Birth;
    }
    public void setGender(String Gender){
        this.Gender = Gender;
    }
    public String getGender(){
        return this.Gender;
    }
    public void setContact_No(String Contact_No){
        this.Contact_No = Contact_No;
    }
    public String getContact_No(){
        return this.Contact_No;
    }
    public void setCustomerId(String customerId){
        this.customerId = customerId;
    }
    public String getCustomerId(){
        return this.customerId;
    }
    public void setKit_No(String Kit_No){
        this.Kit_No = Kit_No;
    }
    public String getKit_No(){
        return this.Kit_No;
    }
    public void setCard_Type(String Card_Type){
        this.Card_Type = Card_Type;
    }
    public String getCard_Type(){
        return this.Card_Type;
    }
    public void setEmail_Address(String Email_Address){
        this.Email_Address = Email_Address;
    }
    public String getEmail_Address(){
        return this.Email_Address;
    }
    public void setAddress_Line1(String Address_Line1){
        this.Address_Line1 = Address_Line1;
    }
    public String getAddress_Line1(){
        return this.Address_Line1;
    }
    public void setAddress_Line2(String Address_Line2){
        this.Address_Line2 = Address_Line2;
    }
    public String getAddress_Line2(){
        return this.Address_Line2;
    }
    public void setCity(String City){
        this.City = City;
    }
    public String getCity(){
        return this.City;
    }
    public void setState(String State){
        this.State = State;
    }
    public String getState(){
        return this.State;
    }
    public void setCountry(String Country){
        this.Country = Country;
    }
    public String getCountry(){
        return this.Country;
    }
    public void setPincode(String Pincode){
        this.Pincode = Pincode;
    }
    public String getPincode(){
        return this.Pincode;
    }
    public void setId_Type(String Id_Type){
        this.Id_Type = Id_Type;
    }
    public String getId_Type(){
        return this.Id_Type;
    }
    public void setId_Number(String Id_Number){
        this.Id_Number = Id_Number;
    }
    public String getId_Number(){
        return this.Id_Number;
    }
    public void setExpiry_Date(String Expiry_Date){
        this.Expiry_Date = Expiry_Date;
    }
    public String getExpiry_Date(){
        return this.Expiry_Date;
    }
    public void setKYC_Status(String KYC_Status){
        this.KYC_Status = KYC_Status;
    }
    public String getKYC_Status(){
        return this.KYC_Status;
    }
    public void setUserId(String userId){
        this.userId = userId;
    }
    public String getUserId(){
        return this.userId;
    }
}
