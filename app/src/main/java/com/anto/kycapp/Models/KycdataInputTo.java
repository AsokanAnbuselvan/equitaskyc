package com.anto.kycapp.Models;

import java.io.Serializable;

/**
 * Created by anto on 2/18/2019.
 */

public class KycdataInputTo implements Serializable{
    public KycdataInputTo(){}
    String business,status;



    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}
