package com.anto.kycapp.Models;

public class ResponseTO {
	private Exception exception;
	private Object pagination;

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public Object getPagination() {
		return pagination;
	}
	public void setPagination(Object pagination) {
		this.pagination = pagination;
	}

}