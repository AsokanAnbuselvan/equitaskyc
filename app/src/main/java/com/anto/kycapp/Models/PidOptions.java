package com.anto.kycapp.Models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "PidOptions", strict = false)
public class PidOptions {

    public PidOptions() {
    }

    @Attribute(name = "ver", required = false)
    public String ver;

    @Element(name = "Opts", required = false)
    public Opts Opts;

    @Element(name = "Pi", required = false)
    public Pi Pi;

}
