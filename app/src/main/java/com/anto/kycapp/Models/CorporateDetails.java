package com.anto.kycapp.Models;

import java.util.List;

/**
 * Created by anto on 2/5/2019.
 */

public class CorporateDetails {
    String name,description,bin;


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
