package com.anto.kycapp.Models;

/**
 * Created by anto on 2/26/2019.
 */

public class ValidatecardandkitRequestTo {

    private String kit,last4,business;

    public String getKit() {
        return kit;
    }

    public void setKit(String kit) {
        this.kit = kit;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }
}
