package com.anto.kycapp.Models;

public class ValidateOtpResponseTo extends ResponseTO{
    Boolean result;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }
}
