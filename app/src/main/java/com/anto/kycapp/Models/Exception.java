package com.anto.kycapp.Models;


public class Exception {
	private String detailMessage, errorCode, shortMessage;
	private String[] fieldErrors;

	public String getDetailMessage() {
		return detailMessage;
	}

	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public void setShortMessage(String shortMessage) {
		this.shortMessage = shortMessage;
	}

	public String[] getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(String[] fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getShortMessage() {
		return shortMessage;
	}
}
