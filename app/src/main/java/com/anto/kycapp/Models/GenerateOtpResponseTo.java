package com.anto.kycapp.Models;

public class GenerateOtpResponseTo extends ResponseTO{
    Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        private Boolean success;
        private Boolean isCustomerExists;
        private String customerId;
        private String customerName;
        private String otp;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Boolean getIsCustomerExists() {
            return isCustomerExists;
        }

        public void setIsCustomerExists(Boolean isCustomerExists) {
            this.isCustomerExists = isCustomerExists;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

    }
}
