package com.anto.kycapp.Models;

public class ValidateBusinessResTo extends ResponseTO {

    public Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        String reqpublic_key;
        String resprivate_key;
        String aesKey;

        public String getAes_Key() {
            return aesKey;
        }

        public void setAes_Key(String aes_Key) {
            this.aesKey = aes_Key;
        }

        public String getReqpulic_key() {
            return reqpublic_key;
        }

        public void setReqpulic_key(String reqpulic_key) {
            this.reqpublic_key = reqpulic_key;
        }


        public String getResprivate_key() {
            return resprivate_key;
        }

        public void setResprivate_key(String resprivate_key) {
            this.resprivate_key = resprivate_key;
        }
    }
}
