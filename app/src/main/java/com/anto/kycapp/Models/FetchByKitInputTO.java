package com.anto.kycapp.Models;

public class FetchByKitInputTO {
    String proxyNumber, ekycRefNo,contactNo,firstName,entityId;

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getProxyNumber() {
        return proxyNumber;
    }

    public void setProxyNumber(String proxyNumber) {
        this.proxyNumber = proxyNumber;
    }

    public String getEkycRefNo() {
        return ekycRefNo;
    }

    public void setEkycRefNo(String ekycRefNo) {
        this.ekycRefNo = ekycRefNo;
    }
}
