package com.anto.kycapp.Models;

/**
 * Created by anto on 3/14/2019.
 */

public class CreateorEditKycRequestTo {

    private String customerId;
    private String business;
    private String ekycRefNo;
    private int documents;

    private String id;
    private String authLevel;
    private String userId;

    public String getEkycRefNo() {
        return ekycRefNo;
    }

    public void setEkycRefNo(String ekycRefNo) {
        this.ekycRefNo = ekycRefNo;
    }

    public int getDocuments() {
        return documents;
    }

    public void setDocuments(int documents) {
        this.documents = documents;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthLevel() {
        return authLevel;
    }

    public void setAuthLevel(String authLevel) {
        this.authLevel = authLevel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
