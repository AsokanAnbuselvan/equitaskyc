package com.anto.kycapp.Models;

/**
 * Created by admin on 31-10-2015.
 */
public class OtpkycValidationResponseTo extends ResponseTO {
    Body body;

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public  class  Body {
        Result result;

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }

    }

    public class Result {
       public ValidationResponse validationResponse;

        public ValidationResponse getValidationResponse() {
            return validationResponse;
        }

        public void setValidationResponse(ValidationResponse validationResponse) {
            this.validationResponse = validationResponse;
        }
    }


    public class ValidationResponse{
        boolean valid;
        String requestId;
        String responseCode;
        String responseMessage;
        String description;
        String ekycRefNo;
        String otpReferenceNo;
        String otpVerificationCode;
        String ekycData;

        public String getOtpVerificationCode() {
            return otpVerificationCode;
        }

        public void setOtpVerificationCode(String otpVerificationCode) {
            this.otpVerificationCode = otpVerificationCode;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }


        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public String getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(String responseCode) {
            this.responseCode = responseCode;
        }

        public String getResponseMessage() {
            return responseMessage;
        }

        public void setResponseMessage(String responseMessage) {
            this.responseMessage = responseMessage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getEkycRefNo() {
            return ekycRefNo;
        }

        public void setEkycRefNo(String ekycRefNo) {
            this.ekycRefNo = ekycRefNo;
        }

        public String getOtpReferenceNo() {
            return otpReferenceNo;
        }

        public void setOtpReferenceNo(String otpReferenceNo) {
            this.otpReferenceNo = otpReferenceNo;
        }

        public String getEkycData() {
            return ekycData;
        }

        public void setEkycData(String ekycData) {
            this.ekycData = ekycData;
        }
    }
}
