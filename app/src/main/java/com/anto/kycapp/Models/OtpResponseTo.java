package com.anto.kycapp.Models;

/**
 * Created by admin on 31-10-2015.
 */
public class OtpResponseTo extends ResponseTO {
    Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        boolean success;
        String customerId, customerName, yapCode;

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean isSuccess) {
            this.success = isSuccess;
        }

        public boolean getSuccess() {
            return success;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getYapCode() {
            return yapCode;
        }

        public void setYapCode(String yapCode) {
            this.yapCode = yapCode;
        }

    }
}
